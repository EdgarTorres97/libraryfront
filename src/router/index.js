import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('../views/books/Index.vue')
    },
    {
      path: '/books',
      name: 'books',
      component: () => import('../views/books/Index.vue')
    }
  ]
})

router.beforeEach( async (to) =>{
  const publicPages = ['/books']

})
export default router
