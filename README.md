
# Configuración del proyecto

```sh
npm install
```

## Configuración del proyecto

Sustituir en el archivo src/main.js el valor de la variable window.axios.defaults.baseURL por la URL de la API.

### Compilar proyecto en desarrollo

```sh
npm run dev
```
